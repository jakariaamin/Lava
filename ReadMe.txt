How to Install and Run in Local System:

1. Download and Install Node from https://nodejs.org/dist/v12.14.0/node-v12.14.0-win-x64.zip

2. Open Command Prompt and Run npm install -g @angular/cli

3. Create a Directory called Lava and git clone using command 
git clone https://gitlab.com/jakariaamin/lava-search.git

4. Open a new command prompt into newly created Lava directory and Run npm install

5. From Command Prompt Run ng serve

6. Open the URL http://localhost:4200/ after completion of compile.



Highlighted Features:
1. Detect Current User Location
2. View Map in Modal
3. Address Autocomplete Suggestion usages Radius of 40km from Home/Starting Adderss
4. Shows Travel Distance and Time with Traffic using Google Distance Matrix
5. This Angular Project hosted to Google Firebase https://lavasearch-6a81e.firebaseapp.com (Bonus Point)


APIs and Services Used:
1. Google Map
2. Google AutoComplete
3. Google Distance Matrix
4. Google Geocode
5. Google GeoLocation
6. Angular Material Theme
7. Angular Animations
8. Google Firebase
9. Bootstrap 4
10. Angular HMR (for hot reload)
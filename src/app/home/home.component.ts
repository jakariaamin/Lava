import {AfterViewInit, Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MyUtilsService} from '../shared/my-utils.service';
import {debounceTime} from 'rxjs/operators';
import {MyConstants} from '../shared/constants';
import {RESTService} from '../shared/rest';
import {MatDialog} from '@angular/material/dialog';
import {DialogMapComponent} from '../modal/dialog-map/dialog-map.component';

@Component({
    selector   : 'app-home',
    templateUrl: './home.component.html',
    styleUrls  : ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
    @ViewChild('addressesView', {static: false}) addressesViewRef: ElementRef;
    @ViewChild('resultView', {static: false}) resultViewRef: ElementRef;

    geocoder = new google.maps.Geocoder();

    form: FormGroup;

    formErrors: any = {
        'address_home'   : [],
        'address_visit_1': [],
        'address_visit_2': [],
        'address_visit_3': [],
        'address_visit_4': [],
        'address_visit_5': [],
        'address_visit_6': [],
        'address_visit_7': [],
    };

    // Form Validation Messages
    validationMessages: any = {
        'address_home'   : {
            'required' : 'Home Address is Required',
            'minlength': 'Home Address must be at least 2 characters long.',
            'maxlength': 'Home Address cannot be more than 256 characters long.',
        },
        'address_visit_1': {
            'required' : 'Visiting Address 1 is Required',
            'minlength': 'Visiting Address 1 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 1 cannot be more than 256 characters long.',
        },
        'address_visit_2': {
            'required' : 'Visiting Address 2 is Required',
            'minlength': 'Visiting Address 2 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 2 cannot be more than 256 characters long.',
        },
        'address_visit_3': {
            'required' : 'Visiting Address 3 is Required',
            'minlength': 'Visiting Address 3 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 3 cannot be more than 256 characters long.',
        },
        'address_visit_4': {
            'required' : 'Visiting Address 4 is Required',
            'minlength': 'Visiting Address 4 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 4 cannot be more than 256 characters long.',
        },
        'address_visit_5': {
            'required' : 'Visiting Address 5 is Required',
            'minlength': 'Visiting Address 5 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 5 cannot be more than 256 characters long.',
        },
        'address_visit_6': {
            'required' : 'Visiting Address 6 is Required',
            'minlength': 'Visiting Address 6 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 6 cannot be more than 256 characters long.',
        },
        'address_visit_7': {
            'required' : 'Visiting Address 7 is Required',
            'minlength': 'Visiting Address 7 must be at least 2 characters long.',
            'maxlength': 'Visiting Address 7 cannot be more than 256 characters long.',
        },
    };


    data: any            = [];   // Distance, Time Result Data
    centerLat: number    = null; // Center Point for Search Radius
    centerLng: number    = null; // Center Point for Search Radius
    address_home: number = null;

    constructor(public myUtils: MyUtilsService, public myRest: RESTService, public dialog: MatDialog, public ngZone: NgZone) {

        // Initialize the form
        this.form = new FormGroup({
            address_home   : new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_1: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_2: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_3: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_4: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_5: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_6: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
            address_visit_7: new FormControl('', this.myUtils.validatorCompose(2, 256, true, null)),
        });
        this.form.valueChanges.pipe(debounceTime(500)).subscribe(data => this.onValueChanged(data));

    }

    ngAfterViewInit() {
    }

    // Detect Input Change and Trigger Validation Check
    onValueChanged(formName: string, data ?: any) {
        if (!this[formName]) {
            return;
        }
        const form = this[formName];
        for (const field in this.formErrors) {
            if (this.formErrors.hasOwnProperty(field)) {
                this.formErrors[field] = []; // clear previous error message
                this[formName][field]  = '';
                const control          = form.get(field);
                if (control && control.dirty && !control.valid) {
                    const messages = this.validationMessages[field];
                    for (const key in control.errors) {
                        if (control.errors.hasOwnProperty(key)) {
                            this.formErrors[field].push(messages[key]);
                        }
                    }
                }
            }
        }
    }

    // Check Form Validation Uplon Submission
    onFormReSubmit(formName: string, data ?: any) {
        if (!this[formName]) {
            return;
        }
        const form = this[formName];
        for (const field in this.formErrors) {
            if (this.formErrors.hasOwnProperty(field)) {
                this.formErrors[field] = []; // clear previous error message
                this[formName][field]  = '';
                const control          = form.get(field);
                if (control && !control.valid) {
                    const messages = this.validationMessages[field];
                    for (const key in control.errors) {
                        if (control.errors.hasOwnProperty(key)) {
                            this.formErrors[field].push(messages[key]);
                            control.markAsDirty();
                        }
                    }
                }
            }
        }
    }

    // Form Submit Button Action
    onSubmit() {
        // Reset Travel Time, Distance Data
        this.data = [];

        this.myUtils.printConsole(true, 'log', 'LOG: Submitted value: ', {'value': this.form.value});
        if (this.form.valid) {
            // Call API When the Form is Valid
            this.processLocations();
        } else {
            // If Form is not valid, Check for Validation and Display Alert Message
            this.onFormReSubmit('form');
            let errorMessage = '';
            for (const key in this.formErrors) {
                if (this.formErrors.hasOwnProperty(key) && this.formErrors[key] && this.formErrors[key].length > 0) {
                    // Create Multi Line Form Validation Errors
                    errorMessage = errorMessage + '<br>' + this.formErrors[key];
                }
            }
            this.myUtils.printConsole(true, 'log', 'LOG: checkValidation: formErrors: ', errorMessage);
            // SHow Invalid Form Submit Error Messages
            this.myUtils.showAlertDialog({'width': '300px', data: {'title': 'Alert', 'message': errorMessage}});
        }
    }

    // Process all the address and get travel time, duration
    public processLocations() {
        // Set Starting Location from Home Address Input
        const originCoords: any = {
            lat: this.form.value.address_home.geometry.location.lat(),
            lng: this.form.value.address_home.geometry.location.lng()
        };

        // Loop Through All 7 Destination Address
        Object.keys(this.form.controls).forEach(key => {
            // Set Destination Coordinates for calcuating Distance, Travel Time
            const destinationCoords: any = {
                lat: this.form.value[key].geometry.location.lat(),
                lng: this.form.value[key].geometry.location.lng()
            };
            // Call Google Distance Matrix to get Travel Distance, Travel Time, Travel Time With Traffic
            this.distanceMatrix(originCoords, destinationCoords)
                .then(result => {
                    console.log(`getDistanceMatrix: `, key, result, result.rows[0].elements[0].distance.text);
                    // Store the distance data in Final Output Variable
                    result.formValue = this.form.value[key];
                    this.data.push(result);
                })
                .catch(err => {
                    console.log(`getDistanceMatrix err: `, err);
                });
        });

        // Scroll View To Element
        this.resultViewRef.nativeElement.scrollIntoView({behavior: 'smooth'});
    }

    // Get Google Autocomplete Place Suggestion Value
    public autoCompleteAddress(place: object, formControlName: string) {
        console.log(place);

        this.ngZone.run(() => {
            // Set Form Value for Addresses
            this.form.controls[formControlName].setValue(place);
            if (formControlName === 'address_home') {
                // Set Center Search Point from Home/Starting Address
                this.centerLat = this.form.value.address_home.geometry.location.lat();
                this.centerLng = this.form.value.address_home.geometry.location.lng();
                // Scroll View To Element
                this.addressesViewRef.nativeElement.scrollIntoView({behavior: 'smooth'});
            }
        });
    }

    // Get Current Location of User
    public getCurrentLocation() {
        // Call Geolocation Function
        this.getCurrentPosition()
            .then(pos => {
                console.log(`Positon: ${pos.lng} ${pos.lat}`);

                this.findAddress(pos.lat, pos.lng, this.geocoder)
                    .then(results => {
                        console.log(`findAddress: `, results);
                        // const latLng = [results[0].geometry.location.lat(), results[0].geometry.location.lng()];
                        const result            = results[0] ? results[0] : null;
                        const formatted_address = results[0] ? results[0].formatted_address : null;
                        this.form.controls['address_home'].setValue(result);
                        this.centerLat = this.form.value.address_home.geometry.location.lat();
                        this.centerLng = this.form.value.address_home.geometry.location.lng();
                        this.address_home = formatted_address;
                        this.myUtils.showSnackBar('Current Location Updated.');
                    })
                    .catch(err => {
                        console.log(`findAddress err: `, err);
                        this.myUtils.showSnackBar('Location Update Failed.');
                    });
            })
            .catch(err => {
                console.log(`Positon err: `, err);
                this.myUtils.showSnackBar('Please Allow Location Access For Location Update.');
            });
    }

    // Use Geolocation Service to Fetch Current Latitude, Longitude of User
    public getCurrentPosition(): Promise<any> {
        return new Promise((resolve, reject) => {

            navigator.geolocation.getCurrentPosition(resp => {
                    resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
                },
                err => {
                    reject(err);
                });
        });
    }

    // Find Address from Latitude Longitude
    public findAddress(latitude, longitude, geocoder): Promise<any> {
        const latLng = new google.maps.LatLng(latitude, longitude);
        return new Promise((resolve, reject) => {
            geocoder.geocode({'location': latLng}, (results, status) => {
                if (status === 'OK') {
                    resolve(results);
                } else {
                    reject(new Error('Couldnt\'t find the address ' + latLng));
                }
            });
        });
    }

    // Find Latitude Longitude from Address String
    public findLatLang(address, geocoder): Promise<any> {
        return new Promise((resolve, reject) => {
            geocoder.geocode({'address': address}, (results, status) => {
                if (status === 'OK') {
                    resolve(results);
                } else {
                    reject(new Error('Couldnt\'t find the location ' + address));
                }
            });
        });
    }

    // Distance Matrix Service to Get Distance, Distance with Traffic, Total Travel Time
    public distanceMatrix(originCoords, destinationCoords): Promise<any> {

        return new Promise((resolve, reject) => {
            const origin      = new google.maps.LatLng(originCoords.lat, originCoords.lng);
            const destination = new google.maps.LatLng(destinationCoords.lat, destinationCoords.lng);
            const service     = new google.maps.DistanceMatrixService();
            const date        = new Date();
            date.setDate(date.getDate() + 1);
            const DrivingOptions: google.maps.DrivingOptions = {
                departureTime: date,
                trafficModel : google.maps.TrafficModel.PESSIMISTIC
            };

            service.getDistanceMatrix(
                {
                    origins          : [origin],
                    destinations     : [destination],
                    travelMode       : google.maps.TravelMode.DRIVING,
                    drivingOptions   : DrivingOptions,
                    unitSystem       : google.maps.UnitSystem.METRIC,
                    durationInTraffic: true,
                    avoidHighways    : false,
                    avoidTolls       : false
                }, (response, status) => {
                    if (status !== google.maps.DistanceMatrixStatus.OK || status !== 'OK') {
                        reject(status);
                    } else {
                        resolve(response);
                    }
                });
        });
    }

    // Open Google Map Modal
    public viewMap(type, address) {
        let latitude  = null;
        let longitude = null;
        // let originCoords      = null;
        // let destinationCoords = null;
        switch (type) {
            case 'map':
                latitude  = address.geometry.location.lat();
                longitude = address.geometry.location.lng();
                break;
            case 'navigation': // TODO: Implement
                latitude  = address.formValue.geometry.location.lat();
                longitude = address.formValue.geometry.location.lng();
                /*originCoords      = {
                 lat: address.geometry.location.lat(),
                 lng: address.geometry.location.lng()
                 };
                 destinationCoords = {
                 lat: address.geometry.location.lat(),
                 lng: address.geometry.location.lng()
                 };*/
                break;
            default:
                break;
        }

        const dialogRef = this.myUtils.dialog.open(DialogMapComponent, {
            'width' : '90%',
            'height': '90%',
            data    : {'type': type, 'lat': latitude, 'lng': longitude},
        });

        dialogRef.afterClosed().subscribe(result => {
            this.myUtils.printConsole(true, 'log', 'LOG: showMapModal: afterClosed: ', result);
        });
    }
}

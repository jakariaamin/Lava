import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MySessionService} from '../../shared/my-session.service';

@Component({
    selector   : 'app-dialog-alert',
    templateUrl: './dialog-alert.component.html',
    styleUrls  : ['./dialog-alert.component.sass']
})
export class DialogAlertComponent {
    param: any;

    constructor(public dialogRef: MatDialogRef<DialogAlertComponent>, @Inject(MAT_DIALOG_DATA) public params: any, public mySession: MySessionService) {
        this.param = params;
        console.log('DialogAlertComponent: ', this.param);
    }

    closeDialog() {
        this.dialogRef.close({'data': 'Yes More Data Here'});
    }
}

import { TestBed } from '@angular/core/testing';

import { MySessionService } from './my-session.service';

describe('MySessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MySessionService = TestBed.get(MySessionService);
    expect(service).toBeTruthy();
  });
});

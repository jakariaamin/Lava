import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MyConstants} from './constants';
import {MySessionService} from './my-session.service';
import {MyLang} from './laguage';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DialogProgressComponent} from '../modal/dialog-progress/dialog-progress.component';
import {DialogAlertComponent} from '../modal/dialog-alert/dialog-alert.component';

@Injectable({
    providedIn: 'root'
})
export class MyUtilsService {
    dialogProgress;

    constructor(private router: Router, private location: Location, public snackBar: MatSnackBar, public dialog: MatDialog, private mySession: MySessionService) {
    }

    // Check if Internet Connection Available
    public isOnline(): boolean {
        return navigator.onLine;
    }

    // Print Console. by modifying parameter here, all the console printing can be disabled or hidden in the full system.
    public printConsole(show: boolean, type: string, title: string, message: any) {
        /*message.forEach((dt, i) => {});*/
        if (show === true) {
            switch (type) {
                case 'log':
                    console.log(title, message);
                    break;
                case 'error':
                    console.error(title, message);
                    break;
                default:
                    console.log(title, message);
                    break;
            }
        }
    }

    // Angular URL Routing
    public navigateByUrl(url: any, extras: any) {
        this.router.navigateByUrl(url, extras);
    }

    // Angular URL Routing
    public routerNavigate(url: any[], extras: any) {
        this.router.navigate(url, extras);
    }

    // Go back to previous route
    public navigateGoBack() {
        this.location.back();
    }

    // Angular Form Validations
    public validatorCompose(minLength: number, maxLength: number, required: boolean, pattern: string) {
        const rules: any[] = [];

        if (required) {
            rules.push(Validators.required);
        }
        if (minLength) {
            rules.push(Validators.minLength(minLength));
        }
        if (maxLength) {
            rules.push(Validators.maxLength(maxLength));
        }
        if (pattern) {
            if (pattern === 'email') {
                rules.push(Validators.pattern(('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')));
            }
        }

        return Validators.compose(rules);
    }

    // Format Google MAP Api address array into text
    getAddressText(data): string {
        return (data.address ? data.address + ', ' : '') + (data.city ? data.city['name'] + ', ' : '') + (data.state ? data.state['name'] + ', ' : '') + (data.country ? data.country['name'] + ', ' : '') + (data.post_code ? data.post_code : '');
    }

    // Show Angular Material Progress Dialog
    public showProgressDialog() {
        if (this.dialogProgress) {
            this.dialogProgress.close();
        } else {
            this.dialogProgress = this.dialog.open(DialogProgressComponent, {
                data: {},
            });
        }

        this.dialogProgress.afterClosed().subscribe(result => {
            this.printConsole(true, 'log', 'LOG: dialogProgress: afterClosed: ', result);
        });
    }

    // Show Angular Material Alert Dialog
    public showAlertDialog(config: any) {
        const dialogAlert = this.dialog.open(DialogAlertComponent, config);
        dialogAlert.afterClosed().subscribe(result => {
            this.printConsole(true, 'log', 'LOG: showAlertDialog: afterClosed: ', result);
        });
    }

    // Show Angular Material Snack Bar
    public showSnackBar(message: string, duration: number = 3000) {
        this.snackBar.open(message, 'Close', {
            duration: duration,
        });

        /*snackBarRef.afterDismissed().subscribe(() => {
         this.printConsole(true, 'log', 'LOG: dialogAlert: afterClosed: ', result);
         });

         snackBarRef.onAction().subscribe(() => {
         this.printConsole(true, 'log', 'LOG: dialogAlert: afterClosed: ', result);

         });

         snackBarRef.dismiss();*/
    }

    // Scroll View To Element
    public scroll(el: HTMLElement) {
        el.scrollIntoView({behavior: 'smooth'});
    }
}

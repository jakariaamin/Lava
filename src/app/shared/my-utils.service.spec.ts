import { TestBed } from '@angular/core/testing';

import { MyUtilsService } from './my-utils.service';

describe('MyUtilsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyUtilsService = TestBed.get(MyUtilsService);
    expect(service).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class MyLang {
    public static NO_DATA_FOUND       = 'No Data Found.';
    public static NO_INTERNET_MESSAGE = 'No Internet Connection.';
}



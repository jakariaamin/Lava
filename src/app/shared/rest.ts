import {Injectable} from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, delay, retry, tap} from 'rxjs/operators';
import {MyUtilsService} from './my-utils.service';
import {MyLang} from './laguage';

@Injectable({
    providedIn: 'root'
})
export class RESTService {
    // Set Default API call header
    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(private http: HttpClient, private myUtils: MyUtilsService) {
    }

    // REST API CALLS:
    public httpAuth(httpType: string, apiURL: string, body: any, headers: any, delaySec: number, retryCount: number, showLoader: boolean): Observable<any> {
        try {
            // Check if Internet Connection Available
            const isOnline = this.myUtils.isOnline();
            if (isOnline !== true) {
                this.myUtils.printConsole(true, 'log', 'LOG: ', {'isOnline': isOnline});
                // Show message if internet connection not available and stop API calling
                this.myUtils.showSnackBar(MyLang.NO_INTERNET_MESSAGE);
                return of(false);
            }
            /*const isAuthenticated = this.isAuthenticated();
             if (isAuthenticated !== true) {
             this.printConsole('log', 'LOG: ', {'isAuthenticated': isAuthenticated});
             return window.alert('You need to Login.');
             }*/

            // Show progress loader if showLoader === true
            if (showLoader !== false) {
                this.myUtils.showProgressDialog();
            }

            /* let params = null;
             if (httpType && httpType === 'GET') {
             for (const k of Object.keys(body)) {
             params = new HttpParams().set(k, body[k]);
             }
             }*/

            // Set HTTP options and headers from parameters
            const httpOptions = this.httpOptions;
            for (const k of Object.keys(headers)) {
                httpOptions.headers = this.httpOptions.headers.append(k, headers[k]);
            }

            // Print the supplied parameter in API call function
            this.myUtils.printConsole(true, 'log', 'LOG: httpAuth: ', {
                'httpType'           : httpType,
                'apiURL'             : apiURL,
                'body'               : body,
                'headers'            : headers,
                'httpOptions.headers': httpOptions.headers,
                'retryCount'         : retryCount
            });

            // Switch between HTTP method
            switch (httpType) {
                case 'GET':
                    // Return API call result
                    return this.http.get(apiURL, this.httpOptions).pipe(delay(delaySec), retry(retryCount),
                        tap(() => {
                            // Show Loader is Supplied true in parameter
                            if (showLoader !== false) {
                                this.myUtils.showProgressDialog();
                            }
                            /*this.printConsole(true, 'log', 'LOG: tap: ', {
                             'apiURL: ': apiURL,
                             });*/
                        }),
                        // Catch error on failed API calls
                        catchError(this.handleError(apiURL, '', [])));
                    break;
                case 'POST':
                    return this.http.post(apiURL, body, httpOptions).pipe(delay(delaySec), retry(retryCount),
                        tap(() => {
                            if (showLoader !== false) {
                                this.myUtils.showProgressDialog();
                            }
                            /*this.printConsole(true, 'log', 'LOG: tap: ', {
                             'apiURL: ': apiURL,
                             });*/
                        }),
                        catchError(this.handleError(apiURL, '', [])));
                    break;
                case 'PUT':
                    return this.http.put(apiURL, body, this.httpOptions).pipe(delay(delaySec), retry(retryCount),
                        tap(() => {
                            if (showLoader !== false) {
                                this.myUtils.showProgressDialog();
                            }
                            /*this.printConsole(true, 'log', 'LOG: tap: ', {
                             'apiURL: ': apiURL,
                             });*/
                        }),
                        catchError(this.handleError(apiURL, '', [])));
                    break;
                case 'DELETE':
                    return this.http.delete(apiURL, this.httpOptions).pipe(delay(delaySec), retry(retryCount),
                        tap(() => {
                            if (showLoader !== false) {
                                this.myUtils.showProgressDialog();
                            }
                            /*this.printConsole(true, 'log', 'LOG: tap: ', {
                             'apiURL: ': apiURL,
                             });*/
                        }),
                        catchError(this.handleError(apiURL, '', [])));
                    break;
                default:
                    return this.http.request(httpType, apiURL, {
                        body        : JSON.stringify(body),
                        headers     : httpOptions.headers,
                        params      : body,
                        responseType: 'json'
                    }).pipe(delay(delaySec), retry(retryCount),
                        tap(() => {
                            if (showLoader !== false) {
                                this.myUtils.showProgressDialog();
                            }
                            /*this.printConsole(true, 'log', 'LOG: tap: ', {
                             'apiURL: ': apiURL,
                             });*/
                        }),
                        catchError(this.handleError(apiURL, '', [])));
            }
        } catch (e) {
            this.myUtils.printConsole(true, 'log', 'LOG: httpAuth: TRY-CATCH: ', {'url': apiURL, 'e': e});
            // this.myService.showToast('Data failed!', '3000', 'bottom');
            // this.hideSpinner = true;
        }
    }

    // Handle and print HTTP API call error
    private handleError<T>(apiURL = '', operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            let errorMessage = '';
            /*if (error instanceof Response) {
             const body   = error.json() || '';
             const err    = body.error || JSON.stringify(body);
             errorMessage = `${error.status} - ${error.statusText || ''} ${err}`;
             }*/
            if (error.error instanceof ErrorEvent) { // Get client-side error
                errorMessage = error.error.message;
                this.myUtils.printConsole(true, 'log', 'LOG: handleError: ', {
                    'apiURL: '      : apiURL,
                    'operation: '   : operation,
                    'errorMessage: ': errorMessage,
                    'error: '       : error
                });
            } else { // Get server-side error
                errorMessage = error.message;
                this.myUtils.printConsole(true, 'log', 'LOG: handleError: ', {
                    'apiURL: '      : apiURL,
                    'operation: '   : operation,
                    'errorMessage: ': errorMessage,
                    'error: '       : error
                });
                // console.log(`LOG: handleError: , ${error.status}\napiURL: ${apiURL}\nOperation: ${operation}\nMessage: ${errorMessage}`);
            }
            // window.alert(errorMessage);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}

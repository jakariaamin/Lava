import {Component, ViewChild, EventEmitter, Output, OnInit, AfterViewInit, Input} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

@Component({
    selector: 'AutocompleteHomeComponent',
    template: `
        <input type = "text" class = "form-control" [(ngModel)] = "autocompleteHomeInput" (ngModelChange) = "autocompleteHomeInputChange.emit(autocompleteHomeInput)" #addressHometext>
    `,
})
export class AutocompleteHomeComponent implements OnInit, AfterViewInit {
    @Output() setAddressHome: EventEmitter<any> = new EventEmitter();
    @ViewChild('addressHometext', {static: false}) addressHometext: any;

    @Input() autocompleteHomeInput: string;
    @Output() autocompleteHomeInputChange = new EventEmitter<string>();

    queryWait: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.getPlaceAutocompleteHome();
    }

    private getPlaceAutocompleteHome() {
        const autocomplete = new google.maps.places.Autocomplete(this.addressHometext.nativeElement,
            {
                componentRestrictions: {country: 'MY'},
                types                : ['establishment'],  // 'establishment' / 'address' / 'geocode'
            });

        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            const place = autocomplete.getPlace();
            this.invokeEvent(place);
        });
    }

    invokeEvent(place: object) {
        this.setAddressHome.emit(place);
    }
}
